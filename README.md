# Clean Homework Data #

This is small java program written in 3 different styles of abstraction whose goal is to demonstrate differences, advantages and disadvantages.

### The exercise goes like this: ###

1.- Divide the group in 3 teams, each team can select one of the programs, each program is one of the Java Classes. Make sure you tell them how many lines of code each class has.

2.- Ask them to realize the exercises in the class comments. Take time of how long it takes to each team to complete each step of the exercise.

3.- Discuss the findings between the class.

Notes: This is a real program that connects with a database and makes changes on it. However the purpose is not to make it work so you don't have to setup a Database. The exercise only needs you to figure out and idea of what the purpose of the program is and the algorithm it follows.

### The goal of the exercise ###

Sometimes programmers have a hard time understanding new concepts because they don't really understand the value of it, they already know how to do it in some "easier way". This exercise is an attempt to show newcomers what the long run advantages of quality code are. 

Some but not all features in this exercise:

- The use of constants as sugar syntax.
- Following naming conventions and breaking the rules of naming conventions when it will help you more not to follow them.
- Different levels of abstraction, advantages and disadvantages of each.

PD: Although not related to the goal of this exercise, the problem of the program is based on an assignment form the course "M101J: MongoDB for Java Developers" which you can take for free at https://university.mongodb.com.