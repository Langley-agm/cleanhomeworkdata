package com.smartassdragon.cleanhomeworkdata;

import com.mongodb.*;

import java.net.UnknownHostException;

/**
 *
 *  CleanHomeworkData_Medium - Approximately 31 Lines of Code.
 *
 *  A program that helps teachers clean up their students' homework data.
 *
 *  Exercise - Answer the following (take note of how long does the exercise takes you):
 *
 *  1- What's the one task this program performs? / What exactly does it do? - resume it in one sentence.
 *  2- How many lines of code you had to read before finding it out?
 *  3- Can you explain the algorithm?
 *  4- How many lines of code you had to read before finding it out?
 *
 *  Notes: Take time of how long does the exercise takes you.
 *  Tip: What's the entry point of any Java program?
 *
 *  @author Langley-agm
 *
 */
public class CleanHomeworkData_Medium {

    public static void main( String[] args ) throws UnknownHostException {

        DB studentsDB = new MongoClient().getDB("students");

        DBCollection gradesCollection = studentsDB.getCollection("grades");

        DBCursor homeworkGrades = gradesCollection.find( new BasicDBObject("type","homework") );

        DBObject sortCriteria = BasicDBObjectBuilder.start( "student_id", 1 ).append( "score", 1 ).get(); //Criteria: Student Ascending then Score Ascending

        DBCursor sortedHomeworkGrades = homeworkGrades.sort(sortCriteria);

        String studentFromLastHW = null;

        while ( sortedHomeworkGrades.hasNext() ){

            String studentFromCurrentHW = sortedHomeworkGrades.next().get("student_id").toString();

            if( !studentFromCurrentHW.equals(studentFromLastHW) ) gradesCollection.remove( sortedHomeworkGrades.curr() );

            studentFromLastHW = studentFromCurrentHW;

        }

        System.out.println("Success");

    }

}