package com.smartassdragon.cleanhomeworkdata;

import com.mongodb.*;

import java.net.UnknownHostException;

/**
 *
 *  CleanHomeworkData_Long - 82 Lines of Code.
 *
 *  A program that helps teachers clean up their students' homework data.
 *
 *  Exercise - Answer the following (take note of how long does the exercise takes you):
 *
 *  1- What's the one task this program performs? / What exactly does it do? - resume it in one sentence.
 *  2- How many lines of code you had to read before finding it out?
 *  3- Can you explain the algorithm?
 *  4- How many lines of code you had to read before finding it out?
 *
 *  Notes: Take time of how long does the exercise takes you.
 *  Tip: What's the entry point of any Java program?
 *
 *  @author Langley-agm
 *
 */
public class CleanHomeworkData_Long {

    private static final String SUCCESS_MSG = "Success !";
    private static final GradesDAO GRADES_DAO = new GradesDAOImpl();
    private static final DBObject STUDENT_ASCENDING_AND_SCORE_ASCENDING_CRITERIA = BasicDBObjectBuilder.start( "student_id", 1 ).append( "score", 1 ).get();

    public static void main( String[] args ) throws UnknownHostException {

        removeTheLowestScoredHomeworkOfEachStudent();

        System.out.println(SUCCESS_MSG);

    }

    private static void removeTheLowestScoredHomeworkOfEachStudent() throws UnknownHostException {

        DBCursor sortedHomeworkGrades = GRADES_DAO.getAllHomework().sort(STUDENT_ASCENDING_AND_SCORE_ASCENDING_CRITERIA);

        String studentFromPreviousHW = null;
        while ( sortedHomeworkGrades.hasNext() ){
            String studentFromCurrentHW = sortedHomeworkGrades.next().get("student_id").toString();

            if( !studentFromCurrentHW.equals(studentFromPreviousHW) ) GRADES_DAO.remove(sortedHomeworkGrades.curr());

            studentFromPreviousHW = studentFromCurrentHW;
        }

    }

    private static interface GradesDAO{

        DBCursor getAllHomework() throws UnknownHostException ;

        WriteResult remove(DBObject _id) throws UnknownHostException ;

        public static final String GRADES_COLLECTION_NAME = "grades";
        public static class Type{
            public static final String HOMEWORK = "homework";
        }

    }

    private static class GradesDAOImpl implements GradesDAO{

        private static final String GRADE_TYPE_PROPERTY = "type";

        private static final DBObject HOMEWORK_TYPE_CRITERIA = new BasicDBObject( GRADE_TYPE_PROPERTY, Type.HOMEWORK );

        private DBCollection getGrades() throws UnknownHostException {
            return DBConnection.get().getCollection(GRADES_COLLECTION_NAME);
        }

        @Override
        public DBCursor getAllHomework() throws UnknownHostException {
            return getGrades().find(HOMEWORK_TYPE_CRITERIA);
        }

        @Override
        public WriteResult remove(DBObject grade) throws UnknownHostException {
            return getGrades().remove(grade);
        }

    }

    private static class DBConnection{

        private static final String STUDENTS_DB_NAME = "students";

        private static MongoClient dbServer;
        private static DB db;

        private DBConnection(){}

        public static DB get() throws UnknownHostException {
            if( dbServer == null ) dbServer = new MongoClient();
            if( db == null ) db = dbServer.getDB(STUDENTS_DB_NAME);
            return db;
        }

    }

}